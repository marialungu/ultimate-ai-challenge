# Changelog

## Wanted changes

- [ ] Implement remaining wizard steps

- [ ] Add State Management (e.g. Apollo/Redux)

- [ ] Refactor SCSS 

- [ ] Extract Wizard Form in a separate, more generic Component, to improve performance

- [ ] Add animations

- [ ] Error Handling


## Version 1.0.0

#### Version 0.5.0

* Add copy text (ditch Lorem Ipsum)
* Added tests

#### Version 0.4.1

* Refactored functions 

#### Version 0.4.0

* Added Cancel functionality 
* Added Landing page
* Added step 1 & 2 screen 

#### Version 0.3.0

* Added Select All functionality
* Added card selection functionality
* Linked Intent Screen to data
* Added reusable button component
* Added intent card component
* Added screen for step 2 

#### Version 0.2.0

* Created  Wizard Header and functionality
* Added needed form steps 

#### Version 0.1.0

* Added basic wizard functionality 
* Added basic project setup 
