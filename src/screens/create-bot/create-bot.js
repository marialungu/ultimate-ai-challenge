import React from 'react'
import { useHistory } from 'react-router-dom'
import Button from "../../components-styled/button/button";

import './create.scss'
import logo from "../../img/ultimate.png";

const CreateBotScreen = () => {
    const history = useHistory()
    const handleCreate = () => {
        history.push('/create-bot')
    }
    return (
        <div className="container-landing" >
            <div className="sidebar">
                <img className="logo" src={logo} />
                <div className="intro">
                    <div className="intro-text">
                        Hi! We are happy that you chose to start automating your business with our help!
                    </div>
                    <div className="intro-text">
                        Let's start by creating your first helpbot!
                    </div>
                </div>
            </div>
            <div className="content">
                <div className="hero-text">
                    Start your journey with automation
                </div>
                <div className="button-container"><Button onClick={handleCreate} type="primary" label="Get started" size="large" /></div>
            </div>
        </div>
    )
}

export default CreateBotScreen
