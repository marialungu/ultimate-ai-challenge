const BASE_URL = 'http://localhost:3001'
const INTENTS = '/intents'
const CHATBOT = '/chatbots'

export const getIntents = async () => {
    let resp =  await fetch(BASE_URL + INTENTS)
    return await resp.json();
}

export const createBot = async (data) => {
    let resp = await fetch(BASE_URL + CHATBOT, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(data)
    });
    return await resp.json();
}
