
const { getExpressions, getReply, getAllIds, findCurrentStep } = require('./utils');

const mockedIntent = {
    trainingData: {
        expressionCount: 170,
        expressions: [
            {
                id: "12",
                text: "string 1"
            },
            {
                id: "34",
                text: "string 2"
            },
            {
                id: "56",
                text: "string 3"
            }
        ]
    },
    reply: {
        id: "78",
        text: "string"
    }
}

const mockedIntentNoExpressions = {
    trainingData: {
        expressionCount: 170,
        expressions: []
    },
    reply: {}
}

const mockedIntentEmpty = {
    trainingData: {},
    reply: {}
}

const mockedIntents = [
    {
        description: "The user says hello.",
        icon: "greeting",
        id: "12",
        name: "Greeting",
        reply: "'Hello :) How can I help you?'",
        trainingData: "'Hello' , 'Hi' , 'Good morning!'"
    },
    {
        description: "The user says goodbye.",
        icon: "goodbye",
        id: "34",
        name: "Goodbye",
        reply: "'Goodbye, have a nice day!'",
        trainingData: "'Thanks, bye!' , 'Goodbye!' , 'See you'"
    }
]

const mockedSteps = [
    {
        name: 'Step 1',
        title: 'Title',
        instructions: 'Please add instructions',
        label: 'Some label',
        description: 'Some description',
        stepIndex:  1,
        Component: 'Step1'
    },
    {
        name: 'Step 2',
        title: 'Title',
        instructions: 'Please add instructions',
        label: 'Some label',
        description: 'Some description',
        stepIndex:  2,
        Component: 'Step2'
    },
]

describe('Utils getExpressions', () => {
    it('SHOULD return a string with all the expressions in an intent, concatenated', () => {
        const expected = "'string 1' , 'string 2' , 'string 3'"
        const result = getExpressions(mockedIntent)
        expect(result).toBe(expected)
    });

    it('SHOULD return an empty string when no expressions are provided', () => {
        const expected = "''"
        const result = getExpressions(mockedIntentNoExpressions)
        expect(result).toBe(expected)
    });

    it('SHOULD return an empty string, when no trainingData is provided', () => {
        const expected = "''"
        const result = getExpressions(mockedIntentEmpty)
        expect(result).toBe(expected)
    });

    it('SHOULD return an empty string, when no intents are provided', () => {
        const expected = "''"
        const result = getReply([])
        expect(result).toBe(expected)
    });
})

describe('Utils getReply', () => {
    it('SHOULD return a string representing the reply for an intent', () => {
        const expected = "'string'"
        const result = getReply(mockedIntent)
        expect(result).toBe(expected)
    });

    it('SHOULD return an empty string, when no reply is provided', () => {
        const expected = "''"
        const result = getReply(mockedIntentEmpty)
        expect(result).toBe(expected)
    });

    it('SHOULD return an empty string, when no intents are provided', () => {
        const expected = "''"
        const result = getReply([])
        expect(result).toBe(expected)
    });
})

describe('Utils getAllIds', () => {
    it('SHOULD return array of ids from intents', () => {
        const expected = [{ id: "12" }, { id: "34" }]
        const result = getAllIds(mockedIntents)
        expect(result).toStrictEqual(expected)
    });

    it('SHOULD return an empty array, when no data is provided', () => {
        const expected = []
        const result = getAllIds([])
        expect(result).toStrictEqual(expected)
    });
})

describe('Utils findCurrentStep', () => {
    it('SHOULD return the  current step, based on index = 2', () => {
        const expected = {
            name: 'Step 2',
            title: 'Title',
            instructions: 'Please add instructions',
            label: 'Some label',
            description: 'Some description',
            stepIndex:  2,
            Component: 'Step2'
        }

        const result = findCurrentStep(2)(mockedSteps)
        expect(result).toStrictEqual(expected)
    })
})
