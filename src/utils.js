import {find, join, map, pathOr, pick, propEq, pipe} from "ramda";

// finds the expressions in each intent (trainingData -> expressions -> text)
// and returns them like " 'string 1' , 'string 2' , 'string 3' "
export const getExpressions = (entry) => {
    return "'" + pipe(pathOr('', ['trainingData', 'expressions']), map(pathOr('', ['text'])), join("' , '"))(entry) + "'"
}

const randomIdGenerator = () => parseInt(Math.random() * 10 ** 10, 10)

// finds the reply in each intent (reply -> text)
// and returns it like " 'string' "
export const getReply = (intent) => "'" + pathOr('', ['reply', 'text'])(intent) + "'";

export const getAllIds = map(pick(['id']))

export const findCurrentStep = (stepIndex) => find(propEq('stepIndex', stepIndex))

export const normalizeIntents = (intents = []) => {
    return intents.map(intent => ({
        ...intent,
        reply: getReply(intent),
        trainingData: getExpressions(intent)
    }))
}

export const encodeBot = ({ selectedItems }) => {
    return ({
        userId: randomIdGenerator(),
        selectedItems,
        botName: 'Spock',
        companyProfile: 'interstellar exploration'
    })
}


