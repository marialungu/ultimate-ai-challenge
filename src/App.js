import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import './App.css';
import CreateBotScreen from "./screens/create-bot/create-bot";
import WizardForm from "./components/wizard-form/wizard-form";

function App() {
  return (
      <Router>
        <div>
          <Switch>
            <Route exact path="/">
              <CreateBotScreen />
            </Route>
            <Route path="/create-bot">
              <WizardForm />
            </Route>
          </Switch>
        </div>
      </Router>
  );
}

export default App;
