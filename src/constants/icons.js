import {FaRegHandPeace} from "react-icons/fa";
import {FaRegHandSpock} from "react-icons/fa";
import {FaRegSmileBeam} from "react-icons/fa";
import {FaRegDizzy} from "react-icons/fa";
import {FaRegGrinHearts} from "react-icons/fa";
import {FaRobot} from "react-icons/fa";
import {FaMale} from "react-icons/fa";
import {FaRegWindowClose} from "react-icons/fa";
import {FaDoorOpen} from "react-icons/fa";
import {FaDoorClosed} from "react-icons/fa";

export default {
    greeting: <FaRegHandPeace />,
    goodbye: <FaRegHandSpock />,
    affirmative: <FaRegSmileBeam />,
    negative: <FaRegDizzy />,
    thanks: <FaRegGrinHearts />,
    bot: <FaRobot />,
    human: <FaMale />,
    login: <FaRegWindowClose />,
    open: <FaDoorOpen />,
    close: <FaDoorClosed />
}


