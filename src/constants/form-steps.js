import InitializeBot from "../components/form-pages/initialize/initialize-bot";
import FinalizeBot from "../components/form-pages/finalize/finalize-bot";
import SelectIntentsContainer from "../components/form-pages/intents/select-intents-container";

export default [
    {
        name: 'To begin ...',
        title: 'Everyone needs a name and a profile. Give your new helpbot a desired name and tell him about your company\'s domain.',
        label: 'Initialize your helpbot',
        description: 'Start by personalizing your helpbot. Give it a name and tell it about your business',
        stepIndex:  1,
        Component: InitializeBot
    },
    {
        name: 'Secondly...',
        title: 'Select the desired intents for your helpbot',
        instructions: 'Please select the cards representing all the intents that you want your helpbot to know once he is started.',
        label: 'Teach your helpbot',
        description: 'Time to give your helpbot a starting point! Our helpbot can be already trained with some intents for your users\' needs. You can choose what type of responses you want for your business to begin with, or you can go on and create them or edit them later.',
        stepIndex:  2,
        Component: SelectIntentsContainer
    },
    {
        name: 'You are ready!',
        title: 'Your helpbot is ready to help your business!',
        description: 'You are all set! You can start letting your users speak to  your new helpbot!',
        label: 'Ship your helpbot',
        stepIndex:  3,
        Component: FinalizeBot
    }
]
