import React from 'react'
import {propEq, find} from "ramda";
import logo from '../../img/ultimate.png'
import './wizard.scss'

import steps from '../../constants/form-steps'

const WizardHeader = ({ stepIndex }) => {
    const currentStep = find(propEq('stepIndex', stepIndex))(steps)
    return (
        <>
            <img className="logo" src={logo} />
            <div className="step-meta">
                <div className="step-title white-color">{currentStep.name}</div>
                <div className="step-description white-color">{currentStep.description}</div>
            </div>
            <div className="steps">
                {steps.map(step => {
                    return (
                        <div className="wizard-step">
                            <div className="form-step">
                                <div className={`step-icon ${step.stepIndex === stepIndex && "icon-active"}`}>
                                    <div className={`step-index ${step.stepIndex === stepIndex ? "white-color" : "lightInk-color"}`}>{step.stepIndex}</div>
                                </div>
                                <div className={`step-label ${step.stepIndex === stepIndex ? "white-color" : "lightInk-color"}`}>{step.label}</div>
                            </div>
                            <div className={`vertical-line ${step.stepIndex === stepIndex && "line-active"}`}/>
                        </div>
                    )
                })}
            </div>
        </>
    )
}

export default WizardHeader
