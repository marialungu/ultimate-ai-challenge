import React, {useState} from 'react'
import {useHistory} from "react-router-dom";
import {add, subtract, __} from "ramda";

import steps from '../../constants/form-steps'
import WizardHeader from "./wizard-header";
import './wizard.scss'
import {createBot, getIntents} from "../../server/api";
import {encodeBot, normalizeIntents} from "../../utils";

const WizardForm = () => {
    const history = useHistory()

    const [formState, setFormState] = useState({selectedItems: []})
    const [currentStep, setCurrentStep] = useState(1);

    const handleNext = () => {
        setCurrentStep(add(1));
    };

    const handlePrev = () => {
        setCurrentStep(subtract(__, 1));
    };

    const handleCancel = () => {
        history.push('/')
    }

    const handleSubmit = async () => {
        const botData = encodeBot(formState)
        await createBot(botData)
            .then(() => console.log('successfully created bot'))
            .catch(() =>console.log('noooooo'))
        history.push('/');
    }

    return (
        <div className="container">
            <div className="header">
                <WizardHeader stepIndex={currentStep} />
            </div>
            <div className="step-container">
                {steps.map(step => {
                    return currentStep === step.stepIndex &&
                        <step.Component
                            formState={formState}
                            stepIndex={currentStep}
                            handleNext={handleNext}
                            handlePrev={handlePrev}
                            setFormState={setFormState}
                            handleSubmit={handleSubmit}
                            handleCancel={handleCancel}
                        />
                })}
            </div>
        </div>
    )
}

export default WizardForm
