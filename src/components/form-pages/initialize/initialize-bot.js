import React from "react";
import './initialize.scss'
import Button from "../../../components-styled/button/button";
import Fade from "react-reveal/Fade";
import {findCurrentStep} from "../../../utils";
import steps from "../../../constants/form-steps";

const InitializeBot = (props) => {
    const { handleNext, handleCancel, stepIndex } = props;
    const currentStep = findCurrentStep(stepIndex)(steps)

    return (
        <div className="hero-image-1" >
            <Fade bottom>
                <div className="container-step">
                    <div className="init-description">
                        <div className="text">{currentStep.title}</div>
                    </div>
                    <div className="button-group">
                        <Button onClick={handleCancel} type="secondary" label="Cancel" size="medium" />
                        <Button onClick={handleNext} type="primary" label="Next" size="medium" />
                    </div>
                </div>
            </Fade>

        </div>
    );
};
export default InitializeBot;
