import React from "react";
import '../initialize/initialize.scss'
import Button from "../../../components-styled/button/button";
import Fade from "react-reveal/Fade";
import {findCurrentStep} from "../../../utils";
import steps from "../../../constants/form-steps";

const FinalizeBot = (props) => {
    const { handleSubmit, handlePrev, handleCancel, stepIndex } = props;
    const currentStep = findCurrentStep(stepIndex)(steps)

    return (
        <div className="hero-image-2">
            <Fade bottom>
                <div className="container-step">
                <div className="init-description">
                    <div className="text">{currentStep.title}</div>
                </div>
                <div className="button-group">
                    <Button onClick={handleCancel} type="secondary" label="Cancel" size="medium" />
                    <div className="button-group-mini">
                        <Button onClick={handlePrev} type="primary" label="Previous" size="medium" />
                        <Button onClick={handleSubmit} type="primary" label="Create" size="medium" />
                    </div>
                </div>
                </div>
            </Fade>
        </div>
    );
};
export default FinalizeBot;
