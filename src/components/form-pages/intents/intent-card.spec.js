import React from "react";
import { shallow } from "enzyme";
import { always } from "ramda";
import IntentCard from "./intent-card";
import toJson from "enzyme-to-json";

const mockId = jest.mock("uuid/v4", () => () => "mockId");

const props = {
  id: mockId,
  icon: "greeting",
  name: "Greeting",
  description: "Some description",
  trainingData: "some string",
  reply: "another string",
  checked: false,
  selectCard: always(null),
};

  describe("Intent card snapshot", () => {
    it("Intent Card should render correctly with data provided", () => {
      const {
        selectIntent,
        id,
        icon,
        name,
        description,
        trainingData,
        reply,
        checked,
        index,
      } = props;

      const wrap = shallow(
        <IntentCard
          checked={checked}
          id={id}
          index={index}
          icon={icon}
          name={name}
          description={description}
          trainingData={trainingData}
          reply={reply}
          selectIntent={selectIntent}
        />
      );

      expect(toJson(wrap)).toMatchSnapshot();
    });
  });
