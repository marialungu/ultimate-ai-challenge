import React, {useEffect, useState} from "react";
import {find, findIndex, includes, insert, insertAll, propEq, remove, length, uniq} from "ramda";
import Fade from 'react-reveal/Fade';
import Switch from "react-switch";

import steps from '../../../constants/form-steps'
import './intents.scss'
import Button from "../../../components-styled/button/button";
import IntentCard from "./intent-card";
import {findCurrentStep, getAllIds} from "../../../utils";

const SelectIntents = (props) => {
    const { formState, setFormState, setShow, show, handleNext, handlePrev, stepIndex, intents, handleCancel } = props;
    const [checked, setChecked] = useState(false)

    const allIds = getAllIds(intents);
    const items = formState.selectedItems

    const currentStep = findCurrentStep(stepIndex)(steps)

    useEffect(() => {
        if (typeof window !== "undefined") {
            window.onscroll = () => {
                let currentScrollPos = window.pageYOffset;
                let maxScroll = document.body.scrollHeight - window.innerHeight;
                if (currentScrollPos >= 0 && currentScrollPos <= maxScroll) {
                    setShow(true)
                } else {
                    setShow(false)
                }
            }
        }
    }, [])

    useEffect(() => {
        checked
            ? setFormState({
                ...formState,
                selectedItems: uniq(insertAll(0, allIds, items)),
            })
            : setFormState({
                ...formState,
                selectedItems: remove(0, length(items), items),
            })
    }, [checked])

    const removeIntent = (val) => {
        const index = findIndex(propEq('id', val.id))(items)
        setFormState({
            ...formState,
            selectedItems: remove(index, 1, items),
        });
    }

    const addIntent = (val) => {
        setFormState({
            ...formState,
            selectedItems: insert(0, val, items),
        });
    }

    const selectIntent = (val) => {
        if(includes(val)(formState.selectedItems)) {
            removeIntent(val)
        } else {
            addIntent(val)
        }
    }

    return (
        <div className="step">
            <div className="step-header">
                <div className="title">{currentStep.title}</div>
                <div className="instructions">{currentStep.instructions}</div>
                <div className="select-all">
                    <div className="select-all-label">Select All</div>
                    <Switch
                        onChange={() => setChecked(!checked)}
                        checked={checked}
                        uncheckedIcon={false}
                        checkedIcon={false}
                        offColor={'#f6f6f6'}
                        onColor={'#F16A54'}
                        height={22}
                        width={45}

                    />
                </div>
            </div>
            <div className="intents">
                {intents.length > 0 && intents.map(({id, icon, name, description, trainingData, reply}, index) => {
                    return (
                        <IntentCard
                            checked={checked}
                            id={id}
                            index={index}
                            icon={icon}
                            name={name}
                            description={description}
                            trainingData={trainingData}
                            reply={reply}
                            selectIntent={selectIntent}
                        />
                    )
                })}
            </div>
            <Fade bottom when={show}>
                <div className="action-bar">
                    <div className="action-button"><Button onClick={handleCancel} type="secondary" label="Cancel" size="medium" /></div>
                    <div className="action-group">
                        <div className="action-button"><Button onClick={handlePrev} type="primary" label="Previous" size="medium" /></div>
                        <div className="action-button"><Button onClick={handleNext} type="primary" label="Next" size="medium" /></div>
                    </div>

                </div>
            </Fade>
        </div>
    );
};
export default SelectIntents;
