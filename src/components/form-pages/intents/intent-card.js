import React, {useEffect, useState} from 'react';
import Icons from "../../../constants/icons";
import Button from "../../../components-styled/button/button";
import './intents.scss'
import {insertAll, length, remove} from "ramda";

const IntentCard = ({ selectIntent, id, icon, name, description, trainingData, reply, checked }) => {
    const [isSelected, setIsSelected] = useState(false)

    useEffect(() => {
        setIsSelected(checked)
    }, [checked])

    const selectCard = (e) => {
        e.preventDefault();
        selectIntent({id})
        setIsSelected(!isSelected)
    }

    return (
        <div className={`intent-card ${isSelected === true ? "intent-selected" : "intent-deselected"}`} onClick={selectCard}>
            <div className="card-header">
                <div className="header-icon">{Icons[icon]}</div>
                <div className="intent-name">{name}</div>
                <div className="intent-description">{description}</div>
            </div>
            <div className="card-content">
                <div>
                    <div className="card-section-header">What you'll say</div>
                    <div className="card-section">{trainingData}</div>
                </div>
                <div>
                    <div className="card-section-header">What he'll reply</div>
                    <div className="card-section">{reply}</div>
                </div>
            </div>
            <div className="card-footer">
                <Button onClick={selectCard} type="primary" label={isSelected ? 'Deselect' : 'Select'} size="small" disabled={isSelected}/>
            </div>
        </div>
    )
}

export default IntentCard
