import React, {useEffect, useState} from 'react';
import {getIntents} from "../../../server/api";
import SelectIntents from "./select-intents";
import {normalizeIntents} from "../../../utils";

const SelectIntentsContainer = (props) => {
    const [intents, setIntents] = useState({})
    const [show, setShow] = useState(false)

    // retrieves intents on component mount
    useEffect(() => {
        getIntents()
            .then(data => normalizeIntents(data))
            .then(allData => setIntents(allData));
    }, [])

    return <SelectIntents {...props} intents={intents} show={show} setShow={setShow}/>
}

export default SelectIntentsContainer
