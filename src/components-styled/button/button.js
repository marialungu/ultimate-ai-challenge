import React from 'react'
import './button.scss'

const Button = ({ type = 'primary', size = 'medium', label, onClick, disabled }) => {

 const buttonStyle = `button-${type} button-${size}`;
 const labelStyle = size === 'large' ? 'label-large' : 'label'

 return (
     <button className={disabled ? `${buttonStyle} button-disabled` : `${buttonStyle}` } onClick={onClick}>
          <div className={labelStyle}>
           {label}
          </div>
     </button>
 )
}

export default Button
