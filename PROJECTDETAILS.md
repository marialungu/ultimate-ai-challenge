# Project details

## UI/UX Design solution

### Styleguide

For this mini app, I decided to go with a minimal design theme, based on Ultimate.ai's branding:

- primary color: `#F16A54`
- secondary color: `#68DEC4`
- tertiary color: `#7152CE`
- dark color: `#172D3D`

As for typography I went with `Mulish`, the closest resemblance to Proxima-Nova

In terms of imagery, I opted for isometric vector illsutration (in lack of geometric vector illustrations) to preserve the sleek, minimal feel of the app

### User Flows

The solution for this challenge was to create a simple wizard form. I made it interesting by adding the wizard header as a side navigation.
Given that the users might not be familiar with the terms and to what is expected from them when filling the form, the sidenav shows a short description of the current step.
Moreover, each page has (or will have) an appropriate title and instructions.

#### Landing Page

The mini app starts with a simple landing page, with a call to action that redirects you to the wizard form 

#### First screen - Initializing your helpbot 

In this step, the user should add a name for the bot, and his company's profile (e.g. e-commerce, healthcare provider) -> implementation TBD

#### Second Screen - Teach your helpbot

In this step, the user should choose what intents he wants his bot to be initialized with.
The given intents are displayed in a grid layout, made of cards. This is an intuitive and clean design, something many users are comfortable with.
A user can select or deselect a card, showing his intention to add or remove the intent in his desired intents list. Moreover, the user can select or deselect all intent cards with a toggle button.

#### Third screen - Ship your helpbot

In this step, the user is ready to hit Create button to start using his helpbot -> Implementation TBD

#### Notes: 

* The wizard form allows you to move between pages
* The wizard form allows you to cancel it in any step, and redirects you to the landing page

## Technical Solution

### Tech used: 

* React
* Ramda
* Scss
* Jest and Enzyme for testing
* Json Server to provide mocked data

### Implementation notes: 

* Added typography, image and color styles as global variables/mixins, in order to use them throughout the app 
* Created a reusable button component, in order to keep the code DRY and style consistent



