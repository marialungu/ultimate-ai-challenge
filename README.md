# Start the project 

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Prerequisites 

Node version (used when developing this app)

    v.15.8.0

## To start app

#### First

Go in `mock-server` directory and run:

    json-server --watch db.json --port=3001

This will start the mock server serving the intents and saving the data from the wizard form

#### Next

Go back in the root and run: 

    yarn start

This will start the UI

## To test app

In order to run the test suites, run:

    yarn test
 
You'll find 4 unit tests for utils functons and 1 snapshot test for Intent Card component

   

## Changes 

You can find all the changes made in this app in [`CHANGELOG.md`](CHANGELOG.md)

## Solution 

You can find more details about the Design Proposal and some implementation notes in [`PROJECTDETAILS.md`](PROJECTDETAILS.md)

## Thank you!

